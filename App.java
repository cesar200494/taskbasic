import java.lang.Math;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.logging.Logger;

class App {

		public BigInteger add(BigInteger  a, BigInteger  b){
			System.out.println("This is add method");
			System.out.println("This is add method for 2 params");
			return a.add(b);
		}
		public BigInteger add(BigInteger  a, BigInteger  b, BigInteger  c){
			System.out.println("This is add method for 3 params");
			return a.add(b).add(c);
		}
		public BigInteger add(BigInteger a, BigInteger b, BigInteger c, BigInteger d){
			System.out.println("This is add method for 4 params");
			return a.add(b).add(c).add(d);
		}
		public BigInteger sub(BigInteger a, BigInteger b){
			return a.subtract(b);
		}
		public BigInteger div(BigInteger a, BigInteger b){
			return a.divide(b);
		}
		public BigInteger mult(BigInteger a, BigInteger b){
			return a.multiply(b);
		}
		public BigDecimal sqrtMethod(BigDecimal a){
			double d = a.doubleValue();
			return new BigDecimal(Math.sqrt(d));
		}
		public static void main (String [] args){
			System.out.println ("You choose " + args[0]);
			System.out.println ("Hello, I'm a new change ");
			App obj = new App ();
			Logger logger =  Logger.getLogger(obj.getClass().getName());
			logger.info("ADD with two Parameters "+ (obj.add(BigInteger.valueOf(10),BigInteger.valueOf(10))));
			logger.info("ADD with tree Parameters "+ (obj.add(BigInteger.valueOf(10),BigInteger.valueOf(10),BigInteger.valueOf(10))));
			logger.info("ADD with Four Parameters "+ (obj.add(BigInteger.valueOf(10),BigInteger.valueOf(10), BigInteger.valueOf(10), BigInteger.valueOf(10))));
			logger.info("SUB with two Parameters "+ obj.sub(BigInteger.valueOf(10),BigInteger.valueOf(3)));
			logger.info("DIV with two Parameters "+ obj.div(BigInteger.valueOf(10),BigInteger.valueOf(2)));
			logger.info("MULT with two Parameters "+ obj.mult(BigInteger.valueOf(10),BigInteger.valueOf(3)));
			logger.info("SQRT with two Parameters "+ obj.sqrtMethod(new BigDecimal (25)));

		}
}